#cPickle

from six.moves import cPickle
f = open('s01.dat', 'rb')
loaded_obj = cPickle.load(f, encoding='bytes')
f.close()
print(loaded_obj)


#Pandas   –  Document @ http://pandas.pydata.org/pandas-docs/stable/io.html#io-pickle

#import pandas as pd
#dataset = pd.read_pickle('data_preprocessed_python\s01.dat')
#print(dataset)
